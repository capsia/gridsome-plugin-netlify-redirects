# Gridsome plugin Netlify redirects

Create redirects for Netlify while building the website.

## Install

```sh
npm i --save gridsome-plugin-netlify-redirects
```

## Setup

```js
{
  use: 'gridsome-plugin-netlify-redirects',
  options: {
    typeName: 'NetlifyRedirects',
    redirects: [
      {
        from: '/blog'
        to: '/store'
        status: 302
      },
      {
        from: '/home'
        to: '/store'
        status: 302
      }
    ]
  }
}
```

You can add more redirects during build to the collection you've defined in the typeName option. Redirects are nodes of the NetlifyRedirects collection by default. You can add nodes using the Gridsome API to the collection while building the website to add more redirects.
