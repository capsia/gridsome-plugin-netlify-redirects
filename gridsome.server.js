const fs = require('fs')
const path = require('path')

class NetlifyRedirects {
    constructor(api, options) {
        api.afterBuild(async ({ config }) => {
            const redirectsCollection = api._app.store.getCollection(options.typeName ? options.typeName : "NetlifyRedirects")
            const redirectsCollectionData = redirectsCollection ? redirectsCollection.data() : []

            let allRedirects = options.redirects
            redirectsCollectionData.forEach(redirect => {
                allRedirects.push({
                    from: redirect.from,
                    to: redirect.to,
                    status: redirect.status
                })
            })

            const redirectFile = this.generateRedirectsFile(allRedirects);
            const filename = path.join(config.outputDir, "_redirects");

            return fs.writeFileSync(filename, redirectFile)
        })
    }

    generateRedirectsFile(redirects) {
        let fileContent = ""
        for (let el of redirects) {
            fileContent += el.from+"    "+el.to+"    "+el.status+"\n"
        }
        return fileContent
    }
}

module.exports = NetlifyRedirects
